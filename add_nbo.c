// gcc add_nbo.c -o add_nbo
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>

/* read 4 bytes from file and 
 * convert to unsigned integer (host byte order)
 */
void fread_and_convert(uint32_t *dst, FILE *fp) {
	uint32_t n;
	int i;

	/* read format is network byte order */
	fread(&n, 1, 4, fp);
	*dst = ntohl(n);
}

int main(int argc, char **argv) {
	FILE *fp1, *fp2;
	uint32_t n1, n2, res;
	int i;

	if (argc != 3) {
		fprintf(stderr, "usage: add_nbo [file1] [file2]\n");
		return 0;
	}
	/* open two files */
	fp1 = fopen(argv[1], "r");
	fp2 = fopen(argv[2], "r");

	/* read and convert to LE */
	fread_and_convert(&n1, fp1);
	fread_and_convert(&n2, fp2);

	res = n1 + n2;
	printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n", n1, n1, n2, n2, res, res);

	fclose(fp1);
	fclose(fp2);
	return 0;
}
